<?php

namespace app\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "UserGender".
 *
 * @property int $uid
 * @property string $gender
 *
 * @property User $u
 */
class UserInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'UserGender';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uid', 'gender', 'username'], 'required'],
            [['uid'], 'default', 'value' => null],
            [['uid'], 'integer'],
            [['gender'], 'string', 'max' => 1],
            [
                ['uid'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => ['uid' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'uid' => 'id',
            'username' => '使用者名稱',
            'gender' => '性別',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getU()
    {
        return $this->hasOne(User::className(), ['id' => 'uid']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'uid']);
    }

}
