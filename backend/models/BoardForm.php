<?php

namespace app\models;

use Yii;
use yii\base\Model;
use common\models\Article;
use common\models\Reply;

class BoardForm extends Model
{
    public $A_ID;
    public $type;

    public function rules()
    {
        return [
            ['A_ID', 'string'],
            ['type', 'string'],
            ['content', 'required'],
        ];
    }

    public function del()
    {
        if (strcmp($this->type, '1') == 0) {
            //刪文
            /*method1:不透過activeRecord直接操作資料庫*/
            /*
            Yii::$app->db
                ->createCommand()
                ->delete('article', ['Article_id' => $this->A_ID])
                ->execute();
            */
            /*method2:使用activeRecord的delete()或deleteAll*/
//            Replier::deleteAll(['reply_id' => [1,2]]);
            /*
            Replier::deleteAll([
                'OR',
                'reply_id = 3',
                'replier_id = 1',
            ]);
            */
            Article::findOne($this->A_ID)->delete();
        } elseif (strcmp($this->type, '2') == 0) {
            //刪回覆
            Reply::findOne($this->A_ID)->delete();
        }

    }
}