<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\bootstrap\ActiveForm;

$this->title = '管理貼文';
$this->params['breadcrumbs'][] = $this->title;
?>

<head>
    <link rel="stylesheet" href="<?php echo Url::to('@web/css/content.css') ?>">
</head>
<body>

<?php foreach ($result as $item): ?>

    <div class='article'>
        <div style="float:right">
            <?php $form = ActiveForm::begin([
                'id' => 'Article' . $item->article_id,
                'fieldConfig' => [
                    'template' => "{input}",
                ],
            ]); ?>
            <?= $form->field($model, 'type')->hiddenInput(['value' => '1']) ?>
            <?= $form->field($model, 'A_ID')->hiddenInput(['value' => $item->article_id]) ?>
            <?= Html::submitButton('刪文', ['class' => 'btn btn-danger',]) ?>
            <?php ActiveForm::end() ?>
        </div>
        <?= Html::encode("#$item->article_id") ?>
        <br><br>
        <?= Html::encode("$item->publisher :") ?>

        <div class='content'>
            <?= Html::encode("$item->content") ?>
        </div>

        <?php foreach ($item->image as $img): ?>
            <?= Html::a(Html::img($img->image, ['class' => 'img']), $img->image, ['target' => '_blank']); ?>
        <?php endforeach; ?>

        <?php foreach ($item->reply as $detail): ?>
            <div class="reply">
                <?php $form = ActiveForm::begin([
                    'id' => 'Reply' . $detail->reply_id,
                    'fieldConfig' => [
                        'template' => "{input}",
                    ],
                    'options' => [
                        'style' => ';'
                    ]
                ]); ?>
                <?= $form->field($model, 'type')->hiddenInput(['value' => '2']) ?>
                <?= $form->field($model, 'A_ID')->hiddenInput(['value' => $detail->reply_id]) ?>
                <?= Html::submitButton('刪留言', ['class' => 'btn btn-danger',]) ?>
                <?= Html::encode("$detail->replier : $detail->replyText"); ?>
                <?php ActiveForm::end() ?>
            </div>
        <?php endforeach; ?>

    </div>
<?php endforeach; ?>
<?= LinkPager::widget(['pagination' => $pagination]); ?>

</body>
