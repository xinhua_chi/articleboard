<?php

namespace app\models;

use Yii;
use yii\base\Model;
use common\models\Article;

class PublishForm extends Model
{
    public $content;
    public $img;

    public function rules()
    {
        return [
            ['content', 'required'],
            [['img'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png,jpg,gif,bmp', 'maxFiles' => 0],
        ];
    }

    public function publish()
    {
        //$a = new Article;
        //var_dump($a->attributeLabels('article_id'));die();
        $new_article = new Article;
        $new_article->saveArticle(Yii::$app->user->identity->id, $this->content, $this->img);
    }
}