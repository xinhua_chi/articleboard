<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\Url;
use common\models\Reply;

class BoardForm extends Model
{
    public $A_ID;
    public $content;
    public $img;

    public function rules()
    {
        return [
            ['A_ID', 'string'],
            [['img'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
            ['content', 'required'],
        ];
    }

    public function reply()
    {
        $new_reply = new Reply;
        $new_reply->saveReply($this->A_ID, Yii::$app->user->identity->id, $this->content);
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->img->saveAs(URl::to("@app/uploads/") . $this->img->baseName . '.' . $this->img->extension);
            return true;
        } else {
            return false;
        }
    }
}