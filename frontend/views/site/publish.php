<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = '發文';
$this->params['breadcrumbs'][] = $this->title;
?>

<head>
    <link rel="stylesheet" href="<?php echo Url::to('@web/css/content.css') ?>">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
</head>
<body>

<?php $form = ActiveForm::begin([
    'action' => ['publish'],
    'fieldConfig' => [
        'template' => "<div>{input}<div>{error}</div></div>",
    ],
]); ?>
<p>在下方輸入文字發文：</p>
<?= $form->field($model, "content")->textarea(["rows" => 10]) ?>
<?= $form->field($model, 'img[]', [
    'template' => '{error}{label}{input}',
    'inputOptions' => ['id' => 'imgBtn'],
])->fileInput([
    'class' => 'file-upload',
    'multiple' => 'true',
])->label("上傳圖片", ['class' => 'btn btn-info',]) ?>
<?= Html::submitButton('發文', [
    'class' => 'btn btn-primary',
]) ?>
<div id="show">

</div>
<!--
<?= Html::tag('p', '', ['class' => 'previewText']) ?>
<?= Html::img("", ['class' => 'preview']) ?>
-->
<?php ActiveForm::end(); ?>

<script>
    function readURL(input) {
        $('#show').empty();
        for (let i = 0; i < input.files.length; i++) {
            let reader = new FileReader();

            reader.onload = function (e) {
                $('#show').append('<p class="previewText">預覽圖: ' + input.files[i].name + '</p>');
                $('#show').append('<img src= "' + e.target.result + '"/>');
            };
            console.log(input.files[i]);
            reader.readAsDataURL(input.files[i]);
        }

    }

    $("#imgBtn").change(function () {
        readURL(this);
        //console.log(this.files.length);
    });
</script>

</body>
