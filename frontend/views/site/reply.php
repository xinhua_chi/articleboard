<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\bootstrap\ActiveForm;

$this->title = '貼文版';
$this->params['breadcrumbs'][] = $this->title;
?>

<head>
    <link rel="stylesheet" href="<?php echo Url::to('@web/css/content.css') ?>">
</head>
<body>

<?php foreach ($result as $item): ?>
    <?php $form = ActiveForm::begin([
        'id' => 'form' . $item->article_id,
        'fieldConfig' => [
            'template' => "{input}",
            'inputOptions' => ['autocomplete' => 'off'],
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>
    <div class='article'>
        <?= Html::encode("#$item->article_id") ?>
        <br><br>
        <?= Html::encode("$item->publisher :") ?>

        <div class='content'>
            <?= Html::encode("$item->content") ?>
        </div>

        <?php foreach ($item->image as $img): ?>
            <?= Html::a(Html::img($img->image, ['class' => 'img']), $img->image, ['target' => '_blank']); ?>
        <?php endforeach; ?>

        <?php foreach ($item->getReply() as $detail): ?>
            <div class="reply">
                <?= Html::encode("$detail->replier : $detail->replyText"); ?>
            </div>
        <?php endforeach; ?>

        <div class='reply'>
            <?= $form->field($model, 'content') ?>
            <?= $form->field($model, 'A_ID')->hiddenInput(['value' => $item->article_id]) ?>
            <?= Html::submitButton('回覆', ['class' => 'btn btn-primary',]) ?>
        </div>

    </div>
    <?php ActiveForm::end() ?>
<?php endforeach; ?>
<?= LinkPager::widget(['pagination' => $pagination]); ?>

</body>
