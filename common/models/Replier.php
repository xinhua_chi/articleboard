<?php

namespace common\models;

use yii\db\ActiveRecord;

class Replier extends ActiveRecord
{
    public static function tableName()
    {
        return '{{Replier}}';
    }
}