<?php

namespace common\models;

use yii\db\ActiveRecord;
use yii\helpers\Url;

class Article extends ActiveRecord
{
    public static function tableName()
    {
        return '{{ArticlePublisher}}';
    }

    public function saveArticle($publisherId, $content, $image)
    {
        $this->savePublisher($publisherId);
        $this->saveContent($content);
        $this->saveImage($image);
    }

    public function delete()
    {
        $this->deleteArticle();
        $this->deleteReply();
    }

    public function getReply()
    {
        return $this->hasMany(Reply::className(), ['article_id' => 'article_id'])->all();
    }

    public function getPublisher()
    {
        return $this->hasOne(User::className(), ['id' => 'publisher_id'])->one()->username;
    }

    public function getContent()
    {
        return $this->hasOne(ArticleContent::className(), ['article_id' => 'article_id'])->one()->content;
    }

    public function getImage()
    {
        return $this->hasMany(ArticleImage::className(), ['article_id' => 'article_id'])->all();
    }

    private function savePublisher($publisherId)
    {
        $this->publisher_id = $publisherId;
        $this->save();
    }

    private function saveContent($content)
    {
        $tmp = new ArticleContent;
        $tmp->article_id = $this->article_id;
        $tmp->content = $content;
        $tmp->save();
    }

    private function saveImage($image)
    {
        if (!empty($image)) {
            foreach ($image as $item) {
                $tmp = new ArticleImage;
                $tmp->article_id = $this->article_id;
                $tmp->image = $this->uploadImg($item);
                $tmp->save();
            }
        }
    }

    private function uploadImg($file)
    {
        $rand = md5(uniqid(rand(), true));
        $file->saveAs(URl::to("@app/web/uploads/") . $rand . '.' . $file->extension);
        return "http://" . $_SERVER['HTTP_HOST'] . Url::base() . '/uploads/' . $rand . '.' . $file->extension;
    }

    private function deleteArticle()
    {
        parent::delete();
        ArticleContent::findOne($this->article_id)->delete();
        if (!is_null($img = ArticleImage::findOne(['article_id' => $this->article_id]))) {
            $img->delete();
        }
    }

    private function deleteReply()
    {
        foreach ($this->reply as $item) {
            $item->delete();
        }
    }
}