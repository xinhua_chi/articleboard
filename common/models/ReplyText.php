<?php

namespace common\models;

use yii\db\ActiveRecord;

class ReplyText extends ActiveRecord
{
    public static function tableName()
    {
        return '{{ReplyText}}';
    }
}