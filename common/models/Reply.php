<?php

namespace common\models;

use yii\db\ActiveRecord;

class Reply extends ActiveRecord
{
    public static function tableName()
    {
        return '{{ReplyArticle}}';
    }

    public function saveReply($articleId, $replierId, $replyText)
    {
        $this->saveArticle($articleId);
        $this->saveReplier($replierId);
        $this->saveReplyText($replyText);
    }

    public function delete()
    {
        parent::delete();
        Replier::findOne($this->reply_id)->delete();
        ReplyText::findOne($this->reply_id)->delete();
    }

    public function getReplier()
    {
        return $this->hasOne(Replier::className(), ['reply_id' => 'reply_id'])->one()
            ->hasOne(User::className(), ['id' => 'replier_id'])->one()->username;
    }

    public function getReplyText()
    {
        return $this->hasOne(ReplyText::className(), ['reply_id' => 'reply_id'])->one()->text;
    }

    private function saveArticle($articleId)
    {
        $this->article_id = $articleId;
        $this->save();
    }

    private function saveReplier($replierId)
    {
        $tmp = new Replier;
        $tmp->reply_id = $this->reply_id;
        $tmp->replier_id = $replierId;
        $tmp->save();
    }

    private function saveReplyText($replyText)
    {
        $tmp = new ReplyText;
        $tmp->reply_id = $this->reply_id;
        $tmp->text = $replyText;
        $tmp->save();
    }
}